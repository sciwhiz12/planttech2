package net.kaneka.planttech2.items.upgradeable;

import net.minecraft.item.Item.Properties;

public class UpgradeableHandItem extends BaseUpgradeableItem
{

	public UpgradeableHandItem(Properties property, int basecapacity, int maxInvSize, float baseAttack, float baseAttackSpeed, int slotId)
	{
		super(property, basecapacity, maxInvSize, baseAttack, baseAttackSpeed, slotId);
	}

}
